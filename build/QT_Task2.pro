QT += widgets opengl

requires(qtConfig(filedialog))

HEADERS       = ../src/mainwindow.h \
                ../src/drawing_area.h

SOURCES       = ../src/main.cpp \
                ../src/drawing_area.cpp \
                ../src/mainwindow.cpp

LIBS += -lopengl32 -lglu32

