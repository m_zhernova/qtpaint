#include <QtWidgets>
#include "mainwindow.h"
#include "drawing_area.h"

MainWindow::MainWindow( QWidget* parent )
   : QMainWindow( parent ), m_drawingArea( new DrawingArea( this ) )
{
   setCentralWidget( m_drawingArea );
   createActions();
   createMenus();
   setWindowTitle( tr( "Task2" ) );
   setFixedSize( 1000, 600 );
}

void MainWindow::closeEvent( QCloseEvent* event )
{
   if ( maybeSave() )
   {
      event->accept();
   }
   else
   {
      event->ignore();
   }
}

void MainWindow::open()
{
   QString fileName = QFileDialog::getOpenFileName( this, tr( "Open File" ), QDir::currentPath() );
   if ( !fileName.isEmpty() )
      m_drawingArea->openImage( fileName );
}

void MainWindow::save()
{
   saveFile();
}

void MainWindow::penColor()
{
   QColor newColor = QColorDialog::getColor( m_drawingArea->penColor() );
   if ( newColor.isValid() )
      m_drawingArea->setPenColor( newColor );
}

void MainWindow::createActions()
{
   m_openAct = new QAction( tr( "&Open..." ), this );
   m_openAct->setShortcuts( QKeySequence::Open );
   connect( m_openAct, &QAction::triggered, this, &MainWindow::open );
   m_saveAct = new QAction( tr( "&Save..." ), this );
   m_saveAct->setShortcuts( QKeySequence::Save );
   connect( m_saveAct, &QAction::triggered, this, &MainWindow::save );
   m_exitAct = new QAction( tr( "&Exit" ), this );
   m_exitAct->setShortcuts( QKeySequence::Quit );
   connect( m_exitAct, &QAction::triggered, this, &MainWindow::close );
   m_penColorAct = new QAction( tr( "&Pen Color..." ), this );
   connect( m_penColorAct, &QAction::triggered, this, &MainWindow::penColor );
   m_clearScreenAct = new QAction( tr( "&Clear Screen" ), this );
   connect( m_clearScreenAct, &QAction::triggered,
           m_drawingArea, &DrawingArea::clearImage );
   m_hideAct = new QAction( tr( "&Hide drawing" ), this );
   connect( m_hideAct, &QAction::triggered,
           m_drawingArea, &DrawingArea::hideDrawing );
   m_showAct = new QAction( tr( "&Show drawing" ), this );
   connect( m_showAct, &QAction::triggered,
           m_drawingArea, &DrawingArea::showDrawing );
}

void MainWindow::createMenus()
{
   m_fileMenu = new QMenu( tr ( "&File" ), this );
   m_fileMenu->addAction( m_openAct );
   m_fileMenu->addAction( m_saveAct );
   m_fileMenu->addSeparator();
   m_fileMenu->addAction( m_exitAct );
   m_optionMenu = new QMenu( tr( "&Options" ), this );
   m_optionMenu->addAction( m_penColorAct );
   m_optionMenu->addSeparator();
   m_optionMenu->addAction( m_clearScreenAct );
   m_optionMenu->addAction( m_hideAct );
   m_optionMenu->addAction( m_showAct );
   menuBar()->addMenu( m_fileMenu );
   menuBar()->addMenu( m_optionMenu );
}

bool MainWindow::maybeSave()
{
   if ( m_drawingArea->isModified() )
   {
      QMessageBox::StandardButton ret;
      ret = QMessageBox::warning( this, tr( "Warning" ),
                                 tr( "The image has been modified.\n"
                                    "Do you want to save your changes?" ),
                                 QMessageBox::Save | QMessageBox::Discard
                                 | QMessageBox::Cancel );
      if ( ret == QMessageBox::Save )
      {
         return saveFile();
      }
      else if ( ret == QMessageBox::Cancel )
      {
         return false;
      }
   }
   return true;
}

bool MainWindow::saveFile()
{
   QString initialPath = QDir::currentPath() + "/untitled.png";
   QString fileName = QFileDialog::getSaveFileName( this, tr( "Save As" ),
                                                   initialPath,
                                                   tr( "%1 Files (*.%2);;All Files (*)" )
                                                   .arg( QString::fromLatin1( "PNG" ) )
                                                   .arg( QString::fromLatin1( "png" ) ) );
   if ( fileName.isEmpty() )
   {
      return false;
   }
   else
   {
      return m_drawingArea->saveImage( fileName, "png" );
   }
}
