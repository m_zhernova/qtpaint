#ifndef SCRIBBLEAREA_H
#define SCRIBBLEAREA_H

#include <QColor>
#include <QGLWidget>
#include <QPoint>
#include <QWidget>

class DrawingArea : public QGLWidget
{
   Q_OBJECT

public:
   DrawingArea( QWidget* parent = nullptr );
   bool openImage( const QString& fileName );
   bool saveImage( const QString& fileName, const char* fileFormat );
   void setPenColor(const QColor& newColor );
   bool isModified() const { return m_modified; }
   QColor penColor() const { return m_myPenColor; }

public slots:
   void clearImage();
   void hideDrawing();
   void showDrawing();

protected:
   void initializeGL() override;
   void resizeGL( int nWidth, int nHeight ) override;
   void paintGL() override;
   void mousePressEvent( QMouseEvent* event ) override;
   void mouseMoveEvent( QMouseEvent* event ) override;
   void mouseReleaseEvent( QMouseEvent* event ) override;

private:
   QPoint m_startPosition;
   QPoint m_endPosition;
   bool m_pressed;
   bool m_backgroung;
   bool m_modified;
   bool m_hidden;
   GLuint m_texture;
   QColor m_myPenColor;
   void resizeImage( QImage* image, const QSize& newSize );
   GLvoid LoadGLTexture( QImage texture1 );
   void setBgnd();
};

#endif
