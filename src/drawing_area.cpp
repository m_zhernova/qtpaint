#include "drawing_area.h"
#include <QGLWidget>
#include <QtWidgets>
#include <gl\gl.h>
#include <gl\glu.h>

DrawingArea::DrawingArea( QWidget* parent )
   : QGLWidget( parent )
{
   setAttribute( Qt::WA_StaticContents );
   m_modified = false;
   m_hidden = false;
   m_myPenColor = Qt::blue;
   setFixedSize( 1000, 600 );
   m_backgroung = false;
}

GLvoid DrawingArea::LoadGLTexture( QImage texture1 )
{
   glGenTextures( 1, &m_texture );
   texture1 = QGLWidget::convertToGLFormat( texture1 );
   glBindTexture( GL_TEXTURE_2D, m_texture );
   glTexImage2D( GL_TEXTURE_2D, 0, 3, ( GLsizei )texture1.width(), ( GLsizei )texture1.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, texture1.bits() );
   glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
   glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
   glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
   glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
   glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL );
}

void DrawingArea::initializeGL()
{
   glEnable( GL_CULL_FACE );
   glEnable( GL_TEXTURE_2D );
   glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );
   glDepthFunc( GL_LESS );
   glEnable( GL_DEPTH_TEST );
   glEnable( GL_COLOR_MATERIAL );
   glEnable( GL_BLEND );
   glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
   glShadeModel( GL_SMOOTH );
   glMatrixMode( GL_PROJECTION );
   glLoadIdentity();
   gluPerspective( 0.0f, ( GLfloat )width() / ( GLfloat )height(), 0.0f, 0.0f );
   glLineWidth( 5 );
}

void DrawingArea::resizeGL( int nWidth, int nHeight )
{
   if ( nHeight == 0 )
      nHeight = 1;
   glViewport( 0, 0, nWidth, nHeight );
   glMatrixMode( GL_PROJECTION );
   glLoadIdentity();
   gluPerspective( 0.0f, ( GLfloat )nWidth / ( GLfloat )nHeight, 0.0f, 000.0f );
   glMatrixMode( GL_MODELVIEW );
}

bool DrawingArea::openImage( const QString& fileName )
{
   if ( !fileName.isEmpty() )
   {
      QImage m_resultImage( this->size(), QImage::Format_RGBA64 );
      m_resultImage.load( fileName );
      m_resultImage = m_resultImage.scaled( this->size(), Qt::KeepAspectRatio );
      QImage fixedImage( this->size(), QImage::Format_RGBA64 );
      QPainter painter;
      painter.begin( &fixedImage );
      painter.setCompositionMode( QPainter::CompositionMode_Source );
      painter.fillRect( fixedImage.rect(), Qt::transparent );
      painter.setCompositionMode( QPainter::CompositionMode_SourceOver );
      painter.drawImage( QPoint( ( this->size().width() - m_resultImage.width() ) / 2,
                               ( this->size().height() - m_resultImage.height() ) / 2 ), m_resultImage );
      painter.end();
      LoadGLTexture( fixedImage );
      m_backgroung = false;
      updateGL();
      m_modified = true;
      return true;
   }
   return false;
}

bool DrawingArea::saveImage( const QString& fileName, const char* fileFormat )
{
   makeCurrent();
   swapBuffers();
   QImage visibleImage = grabFrameBuffer( 0 );
   if ( visibleImage.save( fileName, fileFormat ) )
   {
      m_modified = false;
      makeCurrent();
      swapBuffers();
      return true;
   }
   else
   {
      return false;
   }
}

void DrawingArea::setPenColor( const QColor& newColor )
{
   m_myPenColor = newColor;
}

void DrawingArea::clearImage()
{
   makeCurrent();
   swapBuffers();
   glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
   makeCurrent();
   swapBuffers();
   glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
}

void DrawingArea::hideDrawing()
{
   if( !m_hidden )
   {
      m_hidden = true;
      makeCurrent();
      swapBuffers();
      makeCurrent();
   }
}

void DrawingArea::showDrawing()
{
   if( m_hidden )
   {
      m_hidden = false;
      makeCurrent();
      swapBuffers();
      makeCurrent();
   }
}

void DrawingArea::mousePressEvent( QMouseEvent* event )
{
   if ( event->button() == Qt::LeftButton )
   {
      m_startPosition = event->pos();
      m_pressed = true;
   }
}

void DrawingArea::mouseMoveEvent( QMouseEvent* event )
{
   if ( ( event->buttons() & Qt::LeftButton ) && m_pressed && !m_hidden )
   {
      m_endPosition = event->pos();
      updateGL();
      m_modified = true;
      m_startPosition = m_endPosition;
   }
}

void DrawingArea::mouseReleaseEvent( QMouseEvent* event )
{
   if ( event->button() == Qt::LeftButton && m_pressed )
   {
      m_pressed = false;
   }
}

void DrawingArea::setBgnd()
{
   glEnable( GL_TEXTURE_2D );
   glLoadIdentity();
   glTranslatef( -1.0f, -1.0f, 0.0f );
   glBindTexture( GL_TEXTURE_2D, m_texture );
   glBegin( GL_QUADS );
   glTexCoord2f( 0.0f, 0.0f ); glVertex3f( 0.0f, 0.0f, 0.0f );
   glTexCoord2f( 1.0f, 0.0f ); glVertex3f( 2.0f, 0.0f, 0.0f );
   glTexCoord2f( 1.0f, 1.0f ); glVertex3f( 2.0f, 2.0f, 0.0f );
   glTexCoord2f( 0.0f, 1.0f ); glVertex3f( 0.0f, 2.0f, 0.0f );
   glEnd();
   swapBuffers();
   makeCurrent();
   glLoadIdentity();
   glTranslatef( -1.0f, -1.0f, 0.0f );
   glBindTexture( GL_TEXTURE_2D, m_texture );
   glBegin( GL_QUADS );
   glTexCoord2f( 0.0f, 0.0f ); glVertex3f( 0.0f, 0.0f, 0.0f );
   glTexCoord2f( 1.0f, 0.0f ); glVertex3f( 2.0f, 0.0f, 0.0f );
   glTexCoord2f( 1.0f, 1.0f ); glVertex3f( 2.0f, 2.0f, 0.0f );
   glTexCoord2f( 0.0f, 1.0f ); glVertex3f( 0.0f, 2.0f, 0.0f );
   glEnd();
   swapBuffers();
   makeCurrent();
   glDisable( GL_TEXTURE_2D );
   m_backgroung = true;
}

void DrawingArea::paintGL()
{
   swapBuffers();
   makeCurrent();
   if( !m_backgroung )
   {
      setBgnd();
   }
   if( m_pressed && !m_hidden )
   {
      glDisable( GL_DEPTH_TEST );
      GLfloat x1 = m_startPosition.x();
      GLfloat y1 = height() - m_startPosition.y();
      GLfloat x2 = m_endPosition.x();
      GLfloat y2 = height() - m_endPosition.y();
      x1 = ( GLfloat )2 * x1 / width();
      y1 = ( GLfloat )2 * y1 / height();
      x2 = ( GLfloat )2 * x2 / width();
      y2 = ( GLfloat )2 * y2 / height();
      GLfloat red = ( GLfloat )m_myPenColor.red()/255;
      GLfloat green = ( GLfloat )m_myPenColor. green()/255;
      GLfloat blue = ( GLfloat )m_myPenColor.blue()/255;
      glColor3f( red, green, blue );
      glBegin( GL_LINES );
      glVertex2f( x1, y1 );
      glVertex2f( x2, y2 );
      glEnd();
      makeCurrent();
   }
}

void DrawingArea::resizeImage( QImage* image, const QSize& newSize )
{
   if ( image->size() == newSize )
      return;
   QImage newImage( newSize,  QImage::Format_RGBA64 );
   QPainter painter;
   painter.begin( &newImage );
   painter.setCompositionMode( QPainter::CompositionMode_Source );
   painter.fillRect( newImage.rect(), Qt::transparent );
   painter.setCompositionMode( QPainter::CompositionMode_SourceOver );
   painter.drawImage( QPoint( 0, 0 ), *image );
   painter.end();
   *image = newImage;
}
