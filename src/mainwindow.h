#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class DrawingArea;

class MainWindow : public QMainWindow
{
   Q_OBJECT

public:
   MainWindow( QWidget* parent = nullptr );

protected:
   void closeEvent( QCloseEvent* event ) override;

private slots:
   void open();
   void save();
   void penColor();

private:
   DrawingArea* m_drawingArea;
   QMenu* m_fileMenu;
   QMenu* m_optionMenu;
   QAction* m_openAct;
   QAction* m_saveAct;
   QAction* m_exitAct;
   QAction* m_penColorAct;
   QAction* m_clearScreenAct;
   QAction* m_hideAct;
   QAction* m_showAct;
   void createActions();
   void createMenus();
   bool maybeSave();
   bool saveFile();
};

#endif
